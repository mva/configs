#!zsh

ls_deb() {
	[[ -n "${1}" ]] || return
	local data="$(ar t ${1} | grep data.tar)" x=""
	[[ "${data}" = *xz* ]] && x='J'
	[[ "${data}" = *gz* ]] && x='z'
	ar p "${1}" "${data}" | tar -t"${x}"f-
}

ls_ipk() {
	[[ -n "${1}" ]] || return
	tar -xzOf "${1}" ./data.tar.gz | tar -tzf-
}

