
"""""" Encoding/Multibyte
if has('multi_byte') " If multibyte support is available and
  if &enc !~? 'utf-\=8' " the current encoding is not Unicode,
    if empty(&tenc) " default to
      let &tenc = &enc " using the current encoding for terminal output
    endif " unless another terminal encoding was already set
    set enc=utf-8 " but use utf-8 for all data internally
  endif
endif

