if &enc =~ '^u\(tf\|cs\)' " When running in a Unicode environment,
"  set listchars=tab:   \ ,trail:•,extends:#,nbsp:. "Установить символы которыми будет осуществляться подсветка
  set list " visually represent certain invisible characters:
  let s:arr = nr2char(8250) " using U+203a (    ) for an arrow, and
"  let s:arr = nr2char(9655) " using U+25B7 (▷) for an arrow, and
  let s:dot = nr2char(8901) " using U+22C5 (⋅) for a very light dot,
"  let s:dot = nr2char(8226) " using U+2022 (•) for a very light dot,
" display tabs as an arrow followed by some dots (▷⋅⋅⋅⋅⋅⋅⋅),
  exe "set listchars=tab:" . s:arr . '\ '
" and display trailing and non-breaking spaces as U+22C5 (⋅).
  exe "set listchars+=trail:" . s:dot
  exe "set listchars+=nbsp:" . s:dot
"  exe "set listchars+=precedes:" . nr2char(8617)
"  exe "set listchars+=extends:". nr2char(8618)
  exe "set listchars+=precedes:" . nr2char(8592)
  exe "set listchars+=extends:". nr2char(8594)
endif

