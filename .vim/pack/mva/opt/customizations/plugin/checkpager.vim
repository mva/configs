if !exists("checkpager_loaded")
	let checkpager_loaded = 1

	fun! <SID>check_pager_mode()
"		if exists(":Man") == 2
"			"set statusline=%<MAN:\ %f%h%m%r%=format=%{&fileformat}\ file=%{&fileencoding}\ enc=%{&encoding}\ %b\ 0x%B\ %l,%v\ %P
"			"set statusline=MAN:\ %t\ %l,%v\ %P
"		endif
		if (!has("nvim") && exists(":Man") == 2) || &filetype == 'man' || (exists("g:loaded_less") && g:loaded_less == 1)
		" we're in vimpager / less.sh / man mode
"			set whichwrap=b,s,<,>,[,] " Перемещать курсор на следующую строку при нажатии на клавиши вправо-влево и пр.
"			set laststatus=2
			"set ruler
			let &l:foldmethod='manual' " не сворачивать
			let &l:foldlevel=99 " -//-//-
			let &l:list=0 " не подсвечивать пробелы
			let &l:swapfile=0
			let &l:cursorline=0 " не подсвечивать текущую строку
			let &l:number=0 " не нумервать строки
			let &l:scrolloff=1
			let &l:scrolljump=0
			let &l:colorcolumn=0
			let &l:wrap=1
			set modifiable
			let &l:modifiable=0
			let &l:readonly=1
			let b:statusline_mode_replace="PAGER"
			"if has('autocmd')
			"	au VimEnter * augroup nomodif
			"	au VimEnter * au BufReadPost,BufNewFile * let &l:ma = 0 | let &l:ro = 1
			"	au VimEnter * augroup END
			"endif

		" Disable unuseable keys
			map <buffer> <Insert> <Esc>
			map <buffer> <Delete> <Esc>
			map <buffer> = <Esc>
			map <buffer> i <Esc>
			map <buffer> o <Esc>
			map <buffer> a <Esc>
			map <buffer> s <Esc>
			map <buffer> x <Esc>
			map <buffer> c <Esc>

"			if &wrap
"				noremap <buffer> <SID>L L0:redraw<CR>:echo<CR>
"				normal! L0
"			else
"				noremap <buffer> <SID>L Lg0:redraw<CR>:echo<CR>
"				normal! Lg0
"			endif
"
			if (exists("g:loaded_less") && g:loaded_less == 1) && (has("nvim") || (!has("nvim") && !exists(":Man")))
				set modifiable " somewhy it ignores the one that is set below, so, that's why this is in separate 'if'
				source $MYVIMRC " kludge to re-colorize statusline
				let &l:modifiable=0
				let &l:readonly=1
				let b:statusline_mode_replace="PAGER"
			" Re-read file and page forward "tail -f"
"				map <buffer> F :e<CR>G<SID>L:sleep 1<CR>F
			endif

			if exists(":Man") == 2 && &filetype == 'man'
				let b:statusline_mode_replace="MAN"
				map <buffer> d <Esc>
				map <buffer> u <Esc>
				map <buffer> p <Esc>
				map <buffer> z <Esc>
				map <buffer> <S-f> <Esc>
"				runtime! less.vim
			endif

"		" Scroll one line forward
"			noremap <buffer> <script> <CR> <C-E><SID>L
"			map <buffer> <C-N> <CR>
"			map <buffer> e <CR>
"			map <buffer> <C-E> <CR>
"			map <buffer> j <CR>
"			map <buffer> <C-J> <CR>
"			map <buffer> <Down> <CR>
"
"		" Scroll one page backward
"			noremap <buffer> <script> b <C-B><SID>L
"			map <buffer> <C-B> b
"			map <buffer> <PageUp> b
"			map <buffer> <kPageUp> b
"			map <buffer> <S-Up> b
"			map <buffer> w b
"			map <buffer> <Esc>v b
"
"			noremap <buffer> <script> k <C-Y><SID>L
"			map <buffer> y k
"			map <buffer> <C-Y> k
"			map <buffer> <C-P> k
"			map <buffer> <C-K> k
"			map <buffer> <Up> k
"
"		" Redraw
"			noremap <buffer> <script> r <C-L><SID>L
"			noremap <buffer> <script> <C-R> <C-L><SID>L
"			noremap <buffer> <script> R <C-L><SID>L
"
"		" Start of file
"			noremap <buffer> <script> g gg<SID>L
"			map <buffer> < g
"			map <buffer> <Esc>< g
"			map <buffer> <Home> g
"			map <buffer> <kHome> g
"
"		" End of file
"			noremap <buffer> <script> G G<SID>L
"			map <buffer> > G
"			map <buffer> <Esc>> G
"			map <buffer> <End> G
"			map <buffer> <kEnd> G
"
"		" Search
"			noremap <buffer> <script> / H$:call <SID>Forward()<CR>/
"			if &wrap
"				noremap <buffer> <script> ? H0:call <SID>Backward()<CR>?
"			else
"				noremap <buffer> <script> ? Hg0:call <SID>Backward()<CR>?
"			endif
"
"			fun! s:Forward()
"			" Searching forward
"				noremap <buffer> <script> n H$nzt<SID>L
"				if &wrap
"					noremap <buffer> <script> N H0Nzt<SID>L
"				else
"					noremap <buffer> <script> N Hg0Nzt<SID>L
"				endif
"				cnoremap <buffer> <silent> <script> <CR> <CR>:cunmap <lt>CR><CR>zt<SID>L
"			endfun
"
"			fun! s:Backward()
"			" Searching backward
"				if &wrap
"					noremap <buffer> <script> n H0nzt<SID>L
"				else
"					noremap <buffer> <script> n Hg0nzt<SID>L
"				endif
"				noremap <buffer> <script> N H$Nzt<SID>L
"				cnoremap <buffer> <silent> <script> <CR> <CR>:cunmap <lt>CR><CR>zt<SID>L
"			endfun
		endif
	endfun

	au VimEnter * :call <SID>check_pager_mode()
endif

