" XXX Fix a vim bug: Only t_te, not t_op, gets sent when leaving an alt screen
exe "set t_te=" . &t_te . &t_op

"""""" Titlebar
set title " Turn on titlebar support
set titleold=

"" Set the to- and from-status-line sequences to match the xterm titlebar
"" manipulation begin/end sequences for any terminal where
"" a) We don't know for a fact that these sequences would be wrong, and
"" b) the sequences were not already set in terminfo.
"" NOTE: This would be nice to fix in terminfo, instead...
if &term !~? '^\v(linux|cons|vt)' && empty(&t_ts) && empty(&t_fs)
  exe "set t_ts=\<ESC>]2;"
  exe "set t_fs=\<C-G>"
endif
"
"
if empty($TMUX) && !empty($SSH_TTY)
    let s:hn=substitute(hostname(),"\\([^.]*\\)\\(\..*\\)","\\1","")
    if !empty(s:hn)
        let s:hn=s:hn.":"
    endif
else
    let s:hn=""
endif
"" Titlebar string: hostname> ${PWD:s/^$HOME/~} || (view|vim) filename ([+]|)
let &titlestring=s:hn.'%{&ft=~"^man"?"man":&ro?"view":"vim"}:%{expand("%:t")}'
" .":".expand("%")."]"
if empty(&titleold)
    exe "set titleold=".s:hn."zsh"
endif
"let &titlestring = hostname() . '> ' . '%{expand("%:p:~:h")}'
"                \ . ' || %{&ft=~"^man"?"man":&ro?"view":"vim"} %f %m'
"

