aug CSV_Editing
		au!
		au BufRead *.csv :setfiletype=csv
		au BufRead,BufWritePost *.csv :%ArrangeColumn
		au BufWritePre *.csv :%UnArrangeColumn
aug end
