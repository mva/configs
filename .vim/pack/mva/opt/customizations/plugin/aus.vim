if has("autocmd") && !exists("aus_loaded")
	let aus_loaded = 1
	augroup vimrcEx
	" In plain-text files and svn commit buffers, wrap automatically at 78 chars
	"  au FileType text,svn setlocal tw=78 fo+=t

	" Try to jump to the last spot the cursor was at in a file when reading it.
		au BufReadPost *
\			if ! exists("g:leave_my_cursor_position_alone") |
\				if line("'\"") > 0 && line ("'\"") <= line("$") |
\					exe "normal! g'\"" |
\				endif |
\			endif

lua << LUA
local luv = vim.loop
local api = vim.api
local fn = vim.fn
function on_VimEnter()
	local bufnr = api.nvim_get_current_buf()
	local bufname = api.nvim_buf_get_name(bufnr)
	if not(bufname) or #bufname == 0 then
		bufname = fn.expand('.')
	end
	vim.b.filename = bufname
	local buftype = api.nvim_buf_get_option(bufnr, 'filetype')
	if not(buftype) or #buftype == 0 then
		buftype = fn.join(fn.split(fn.system(("file -s -p -L -z -b --mime-type %s 2>/dev/null"):format(bufname))))
		if buftype:match("cannot open") then
			buftype = "empty"
		end
	end

	vim.b.filetype = buftype
	if buftype:match("directory") then
		local tree = require'nvim-tree'
		vim.o.laststatus=0
		if #api.nvim_buf_get_name(bufnr) > 0 then
			vim.cmd[[bwipeout]]
		end
		require'bufferline.state'.set_offset(31, 'FileTree')
		tree.open()
	end
end

function on_FileType()
	return
end

function on_AnyBufEnter()
function open_url(url)
  vim.g.loaded_netrw=nil
  vim.g.loaded_netrwPlugin=nil
  vim.cmd[[runtime! plugin/netrwPlugin.vim]]
  -- return vim.fn['netrw#BrowseX'](url, vim.fn['netrw#CheckIfRemote']())
  return vim.fn['netrw#FileUrlEdit'](url)
end

	local bufname = api.nvim_buf_get_name(bufnr)
	if not(bufname) or #bufname == 0 then
	end
	vim.b.filename = bufname
	local ft = vim.o.filetype
	local ignore_fts = {
		help = true,
		man = true,
	}
	if ignore_fts[ft] then
		return
	else
		if bufname:match('^%w+://%w+') then
			open_url(bufname)
		else
			api.nvim_set_current_dir(fn.expand("%:p:h"))
		end
	end
	if not(ft == 'NvimTree') then
		vim.o.laststatus=2
		print('\n') -- hide remains after 'laststatus=0'
	else
		require'bufferline.state'.set_offset(31, 'FileTree')
	end
end

function on_AnyBufLeave()
	local ft = vim.o.filetype
	if ft == 'NvimTree' then
		require'bufferline.state'.set_offset(0)
		return
	end
end
LUA


"		au VimEnter * lua on_VimEnter()
"		au FileType * lua on_FileType()

"	" Use :make to syntax check a perl script.
"		au FileType perl set makeprg=perl\ -c\ %\ $* errorformat=%f:%l:%m

"	" Use :make to compile C, even without a makefile
"		au FileType c if glob('[Mm]akefile') == "" | let &mp="clang -o %< %" | endif
"
"	" Use :make to compile C++, too
"		au FileType cpp if glob('[Mm]akefile') == "" | let &mp="clang++ -o %< %" | endif

	" When reading a file, :cd to its parent directory unless it's a help file.
"		au BufEnter * if &ft != 'help' | silent! cd %:p:h | endif
"		au BufEnter * lua on_AnyBufEnter()
"		au BufLeave * lua on_AnyBufLeave()

"" WTF?		au Filetype * let &l:ofu = (len(&ofu) ? &ofu : 'syntaxcomplete#Complete')

		au BufWritePost ~/.Xdefaults redraw|echo system('xrdb '.expand('<amatch>'))

		au FileType help wincmd o | set buflisted " make it "usual buffer"

		" au BufEnter * lua if vim.o.ft == 'NvimTree' then require'bufferline.state'.set_offset(31, 'FileTree') end
		" au BufLeave * lua if vim.o.ft == 'NvimTree' then require'bufferline.state'.set_offset(0) end

		au BufEnter,BufNewFile,BufRead *.sls set ft=yaml sw=2 ts=2 et
		au BufEnter,BufNewFile,BufRead *.yml set ft=yaml sw=2 ts=2 et
		au BufEnter,BufNewFile,BufRead */playbooks/*.ya?ml set filetype=yaml.ansible sw=2 ts=2 et

		au BufEnter ?akefile* set noet nocindent

	" JS {{{
		au BufRead,BufNewFile *.js set ft=javascript.jquery
	" }}}

	" CSS {{{
		au BufRead,BufNewFile *.css set ft=css syntax=css
	" }}}

	" PHP {{{
		au BufRead,BufNewFile *.php setl foldmethod=manual
		au BufRead,BufNewFile *.php setl fen
		let php_sql_query=1	 " подсветка SQL запросов
		let php_htmlInStrings=1 " подсветка HTML кода в PHP строках
		let php_folding=1	   " фолдинг функций и классов
	" }}}

	" DOC {{{
		au BufReadPre *.doc set ro
		au BufReadPre *.doc set hlsearch!
		au BufReadPost *.doc %!antiword "%"
	"}}}

		function s:MkNonExDir(file, buf)
			if empty(getbufvar(a:buf, '&buftype')) && a:file!~#'\v^\w+\:\/'
				let dir=fnamemodify(a:file, ':h')
				if !isdirectory(dir)
					call mkdir(dir, 'p')
				endif
			endif
		endfunction

		augroup BWCCreateDir
			autocmd!
			autocmd BufWritePre * :call s:MkNonExDir(expand('<afile>'), +expand('<abuf>'))
		augroup END

	" Настройка фолдинга для .vimrc и прочих vim файлов
"		au BufRead,BufNewFile,FileType,Syntax vim setl fdm=marker

		au FileWritePost * :unlet b:filetype

		au InsertLeave * call cursor([getpos('.')[1], getpos('.')[2]+1])

"		au BufRead */colors/*.vim let g:colorizer_swap_fgbg=0 " was FileType

		au BufRead *nginx*work*/config set ft=nginx sw=4 ts=4 " nginx modules (and itself) config files in portage tmpdir

	augroup END

"	au BufNewFile,BufRead *.css,*.html,*.htm,*/colors/*.vim,*.php if line("$") < 1000 | if exists(":ColorHighlight") ==# 2 | ColorHighlight! | endif | endif
"	au BufNewFile,BufRead *.css,*.html,*.htm,*/colors/*.vim,*.php if exists(":ColorHighlight") ==# 2 | ColorHighlight! | endif
"	au BufNewFile,BufRead *.css,*.html,*.htm,*/colors/*.vim,*.php if exists(":ColorHighlight") ==# 2 | exec line("w0").",".line("w$")."ColorHighlight!" | let b:colorizer_au_loaded=1 | endif
"	au CursorMoved,CursorMovedI * if exists("b:colorizer_au_loaded") | exec line("w0").",".line("w$")."ColorHighlight!" | endif

	"function s:ColorizeVisibleLines(start,end)
	"	if exists("b:colorizer_au_loaded") | exec line("w0").",".line("w$")."ColorHighlight!" | endif
	"endfunction

	" ^^^
	" Actually, Colorizer makes vim to take too much time to load, but for now
	" it is no way to either make it colorize code without freezing the
	" interface, nor to load it automatically (and not on manual call).
	"
	" I thought about colorizing only visible text on the screen, but it is a
	" bad way too.
	"
	" TODO: flood {neo,}vim developers with request for kind of job API, but
	" for using with internal commands and functions (but not only shell
	" commands)



" buffer=tab (no need to track buffers)
"	au BufNewFile,BufRead * nested tab sball
"\		if &buftype != "help" |
"\			tab sball |
"\		endif


"autocmd FileType css set omnifunc=csscomplete#CompleteCSS
"autocmd FileType php set omnifunc=phpcomplete#CompletePHP

	au VimLeave * set guicursor=a:ver1-Cursor/lCursor-blinkon1

endif

