if &buftype != "help" && !exists("$MANOPT") && ( &filetype != "man" && &filetype != "help") && has("autocmd")
	augroup numbertoggle
		autocmd!
		"autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
		"autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
		autocmd BufEnter,InsertLeave * if &number | set relativenumber | endif
		autocmd BufLeave,InsertEnter * if &number | set norelativenumber | endif
	augroup END
endif

