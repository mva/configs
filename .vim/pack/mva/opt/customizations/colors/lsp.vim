highlight LSPErrorSign guifg=#ee0000 "     "
highlight LSPWarningSign guifg=#eeee00 "  "
highlight LSPInfoSign guifg=#2288ee  "    "
highlight LSPHintSign guifg=#33dd99  "        "

hi link LspDiagnosticsDefaultError LSPErrorSign
hi link LspDiagnosticsDefaultWarning LSPWarningSign
hi link LspDiagnosticsDefaultInformation LSPInfoSign
hi link LspDiagnosticsDefaultHint LSPHintSign

hi link LspDiagnosticsVirtualTextError LSPErrorSign
hi link LspDiagnosticsVirtualTextWarning LSPWarningSign
hi link LspDiagnosticsVirtualTextInformation LSPInfoSign
hi link LspDiagnosticsVirtualTextHint LSPHintSign

hi LspDiagnosticsUnderlineError gui=undercurl
hi LspDiagnosticsUnderlineWarning gui=undercurl
hi LspDiagnosticsUnderlineInformation gui=undercurl
hi LspDiagnosticsUnderlineHint gui=undercurl
" (?) guisp=#00ff00

" hi LspDiagnosticsFloatingError
" hi LspDiagnosticsFloatingWarning
" hi LspDiagnosticsFloatingInformation
" hi LspDiagnosticsFloatingHint

hi link LspDiagnosticsSignError LSPErrorSign
hi link LspDiagnosticsSignWarning LSPWarningSign
hi link LspDiagnosticsSignInformation LSPInfoSign
hi link LspDiagnosticsSignHint LSPHintSign


" FIXME
hi LspReferenceText guibg=#ffff00 guifg=#ff0000 gui=bold
hi LspReferenceRead guibg=#00ffff guifg=#0000ff gui=bold
hi LspReferenceWrite guibg=#00ff00

"sign define DiagnosticSignError         text=   numhl=LspDiagnosticsSignError texthl=LspDiagnosticsSignError
sign define DiagnosticSignError         text=   numhl=LspDiagnosticsSignError texthl=LspDiagnosticsSignError
" linehl=
" text=✗
sign define DiagnosticSignWarn       text=   numhl=LspDiagnosticsSignWarning texthl=LspDiagnosticsSignWarning
" text=⚠ "
sign define DiagnosticSignInfo   text=   numhl=LspDiagnosticsSignInformation texthl=LspDiagnosticsSignInformation
" text=
"sign define DiagnosticSignHint          text=   numhl=LspDiagnosticsSignHint texthl=LspDiagnosticsSignHint
sign define DiagnosticSignHint          text=   numhl=LspDiagnosticsSignHint texthl=LspDiagnosticsSignHint
"sign define DiagnosticSignHint          text=   numhl=LspDiagnosticsSignHint texthl=LspDiagnosticsSignHint
" text=➤
