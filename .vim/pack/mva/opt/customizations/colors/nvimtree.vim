hi NvimTreeSpecialFile guifg=#FFAAFF gui=bold,italic

hi NvimTreeOpenedFolderName  guifg=#FFFF00
hi NvimTreeFolderName        guifg=#FF00FF
hi NvimTreeFolderIcon        guifg=#AA0000

" NvimTreeFolderName
" NvimTreeRootFolder
" NvimTreeFolderIcon
" NvimTreeEmptyFolderName
" NvimTreeOpenedFolderName

