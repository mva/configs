#! /usr/bin/env zsh


## Rewriten almost from scratch. Mostly inspired by Julien Nikolaud's zshrc.
## Less inspired by my many-years-old custom zshrc.


if [ ! -f ~/.zshrc.zwc -o ~/.zshrc -nt ~/.zshrc.zwc ]; then
    zcompile ~/.zshrc
fi

## TODO: ls and ps can be non-GNU (and so break all the things

EPREFIX="${EPREFIX:-${${HOME:A:h:h}%/}}"
[[ ${EPREFIX} == '.' ]] && unset EPREFIX

HOME=${HOME:-~}

test -f /etc/zsh/zprofile && source /etc/zsh/zprofile # Can break the things. Testing...
test -f /etc/profile && {
  emulate bash
  source /etc/profile
  emulate zsh
}
test -f ~/.profile && {
  emulate bash
  source "${HOME}/.profile"
  emulate zsh
}
test -f "${HOME}/.zprofile" && source "${HOME}/.zprofile"

# ------------------------------------------------------------------------------
# Environment / default applications settings
# ------------------------------------------------------------------------------

MOSH_TITLE_NOPREFIX=""

LANG="${LANG:-ru_RU.UTF-8}"
#LC_ALL="${LC_ALL:-ru_RU.UTF-8}"
LC_NUMERIC="${LC_NUMERIC:-POSIX}"
LC_CTYPE=en_US.UTF-8

[[ -z "${TTY}" ]] && (( $+commands[tty] )) && tty -s &>/dev/null && {
  TTY="$(tty 2>/dev/null)"
}

if [[ -n "${TMUX}" ]] {
  # TODO: investigate why on the Earth it sometimes freezes in read
  tmux showenv 2>/dev/null | while {read -t0.1 tmuxvar} {
    [[ "${${tmuxvar#-}%=*}" == "TERM" ]] && continue
    if [[ "${tmuxvar}" == "${tmuxvar#-}" ]] {
      export "${tmuxvar}"
    } else {
      unset "${tmuxvar#-}"
    }
  }
  unset tmuxvar
}

(( $+commands[gpg] )) && (( $+commands[gpg-agent] )) && {
  GPG_TTY=${GPG_TTY:-${TTY}}
  SSH_AUTH_SOCK=${SSH_AUTH_SOCK:-/run/user/${UID}/gnupg/S.gpg-agent.ssh}
  [[ -n "${SSH_CONNECTION}" ]] && {
    export DBUS_SESSION_BUS_ADDRESS=/dev/null GPG_TTY=${TTY} PINENTRY_USER_DATA="USE_CURSES=1"
# To make ssh-agent passwork asking work when I've connected over ssh:
#    gpg-connect-agent updatestartuptty /bye &>/dev/null
# But it brakes UX when I "back" to keyboard. Better to play with SSH_AUTH_SOCK, I think...
  }
}

export HOME LANG LC_NUMERIC LC_CTYPE MOSH_TITLE_NOPREFIX TTY GPG_TTY SSH_AUTH_SOCK PINENTRY_USER_DATA

# System "main" user info.
CONFIG_HOME=${${(%):-%N}:A:h} # Resolve from zshrc location, following symlinks.

# Some GNU settings / PATHs
export BLOCKSIZE=Mb

if (( $+commands[nvim] )) {
  export EDITOR="nvim"
  alias vim="nvim"
  alias vi="nvim"
  alias ed="nvim"
} elif (( $+commands[vim] )) {
  export EDITOR="vim"
} else {
  export EDITOR="nano"
}

export VISUAL=${VISUAL:-${EDITOR}}

# pager
#if (( $+commands[nvimpager] )) { # nah! :-/
PAGER="less"

export LESS='-rMWi~ --tabs=2 --shift 5'
export LESSCHARSET=utf-8
export LESSHISTFILE="${HOME}/.lesshst"
export LESSHISTSIZE=2000

# grep
#export GREP_OPTIONS='--color=auto --exclude="*.pyc" --exclude-dir=".svn" --exclude-dir=".hg" --exclude-dir=".bzr" --exclude-dir=".git"'
export GREP_COLOR='1;31'
export GREP_COLORS='sl=49;39;1:cx=49;39;3:mt=49;38;5;124;1:fn=49;38;5;161;1:ln=49;38;5;208;1;3:bn=49;38;5;99;1;3:se=49;38;5;81;1;3'
### man path
##export MANPATH="/usr/share/man/ru:/usr/man:/usr/share/man:/usr/local/man:/usr/X11R6/man:/opt/qt/doc"

# Some ZSH setings
DIRSTACKSIZE=20
READNULLCMD="cat"
CORRECT_IGNORE="_*"
#ZLE_RPROMPT_INDENT=0
# Word separator characters
WORDCHARS=':*?_-.[]~&;!#$%^(){}<>|'
# History settings.
HISTFILE="${HOME}/.zhistory"
HISTFILESIZE=65536
HISTSIZE=4096
SAVEHIST="${HISTSIZE}"
# User activity reporting.
watch=(notme)
LOGCHECK=10
# Display usage statistics for commands running > 5 sec.
REPORTTIME=5

LISTPROMPT=''
SPROMPT='zsh: заменить '%R' на '%r'? [nyae] '

# commands path.
test -d "${HOME}/.local/bin" && path=("${HOME}/.local/bin" $path)     # local ~/.bin
test -d "${HOME}/.bin" && path=("${HOME}/.bin" $path)                 # local ~/.bin
test -d "${JAVA_HOME:-notexist}/bin" && path=("${JAVA_HOME:-notexist}/bin" $path)         # local java vm
test -d "${CONFIG_HOME}/bin" && path=("${CONFIG_HOME}/bin" $path)     # versioned bin
(( $+commands[gem] )) && [[ -z "${CLS_DISABLE}" ]] && {
  # Generan gems helper
  local gemdir="$(gem environment gemdir &>/dev/null)" # silence in case of broken ruby installation
  test -d ${gemdir} && path=(${gemdir} $path) # Ruby gems

  # ColorLS completion
  local clsdir=${$(gem which colorls 2>/dev/null || echo /not/existent/path/h/h):A:h:h}
  test -d "${clsdir}" && {
    fpath=("${clsdir:A:h}/zsh" $fpath)
    ____cls_loaded=1
  }
  unset gemdir clsdir # not a subshell, so locals anyway goes to i-shell
}
# functions path.
test -d "${CONFIG_HOME}"/.config/zsh/bundles/mva/functions && fpath=("${CONFIG_HOME}"/.config/zsh/bundles/mva/functions/** $fpath)
#
#  # Use zsh-completions if available. (use zsh-completion package on production instead)
#  test -d "${CONFIG_HOME}"/.config/zsh/bundles/mva/apps/zsh-completions && fpath=("${CONFIG_HOME}"/.config/zsh/bundles/mva/apps/zsh-completions/src $fpath)
# cd path
cdpath=( /mnt /media .. )

#typeset -U path cdpath fpath manpath


# Default editors/browsers.
if (( $+commands[firefox] )) {
  export BROWSER="firefox"
} elif (( $+commands[links] )) {
  export BROWSER="links"
} elif (( $+commands[lynx] )) {
  export BROWSER="lynx"
} elif (( $+commands[w3m] )) {
  export BROWSER="w3m"
} else {
  export BROWSER="wget"
}

# TODO: ^^^ `pick-web-browser`

# ----------------------------------------------------------------------------
# Options
# Multiple setopts is just to ease commenting. It doesn't make any
# difference on zsh's work (thx to Mikachu on #zsh@FreeNode).
# ----------------------------------------------------------------------------

setopt ALIASES
setopt ALWAYS_LAST_PROMPT
#setopt APPEND_HISTORY
setopt AUTO_CD
setopt AUTO_CONTINUE
setopt AUTO_LIST
setopt AUTO_NAME_DIRS
setopt AUTO_PARAM_KEYS
setopt AUTO_PARAM_SLASH
setopt AUTO_PUSHD
setopt AUTO_REMOVE_SLASH
setopt AUTO_RESUME
setopt BAD_PATTERN
setopt BANG_HIST
setopt BG_NICE
setopt CASE_MATCH
setopt C_BASES
setopt CDABLE_VARS
setopt CHASE_LINKS
setopt CHECK_JOBS
setopt CLOBBER
setopt COMBINING_CHARS
setopt COMPLETE_ALIASES
setopt COMPLETE_IN_WORD
setopt CORRECT
#setopt CORRECT_ALL #
setopt DEBUG_BEFORE_CMD ##
setopt EMACS ##
setopt EVAL_LINENO ##
setopt EXEC ##
setopt EXTENDED_GLOB
setopt FLOW_CONTROL ##
setopt FUNCTION_ARGZERO ##
setopt GLOB ##
setopt GLOBAL_EXPORT ##
setopt GLOBAL_RCS ##
setopt GLOB_DOTS #
setopt GLOB_COMPLETE
setopt HASH_CMDS #
setopt HASH_DIRS #
setopt HASH_LIST_ALL ##
setopt HIST_EXPIRE_DUPS_FIRST
unsetopt HIST_FCNTL_LOCK ## !! (Disable until 5.0.7 due to double locking bug, which makes zsh to freeze)
setopt HIST_FIND_NO_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_REDUCE_BLANKS
#setopt HIST_SAVE_BY_COPY
setopt HIST_SAVE_NO_DUPS
setopt HIST_SUBST_PATTERN ##
setopt HIST_VERIFY
setopt HUP ##
#setopt INC_APPEND_HISTORY
setopt INTERACTIVE_COMMENTS
setopt LIST_AMBIGUOUS
setopt LIST_PACKED
setopt LIST_ROWS_FIRST #
setopt LIST_TYPES ##
setopt LONG_LIST_JOBS
setopt MAGIC_EQUAL_SUBST #
setopt MARK_DIRS
setopt MENU_COMPLETE
setopt MONITOR ##
setopt MULTIBYTE ##
setopt MULTI_FUNC_DEF ##
#setopt MAIL_WARNING ##
setopt MULTIOS
#unsetopt ALL_EXPORT
#unsetopt ALWAYS_TO_END
##unsetopt BARE_GLOB_QUAL
#unsetopt BASH_AUTO_LIST
#unsetopt BASH_REMATCH
#unsetopt BRACE_CCL
#unsetopt BSD_ECHO
#unsetopt CASE_GLOB
#unsetopt CHASE_DOTS
#unsetopt CORRECT_ALL
#unsetopt C_PRECEDENCES
#unsetopt CSH_JUNKIE_HISTORY
#unsetopt CSH_JUNKIE_LOOPS
#unsetopt CSH_JUNKIE_QUOTES
#unsetopt CSH_NULLCMD
#unsetopt CSH_NULL_GLOB
#unsetopt DVORAK
#unsetopt EQUALS #
#unsetopt ERR_EXIT
#unsetopt ERR_RETURN
#unsetopt EXTENDED_HISTORY
#unsetopt GLOB_ASSIGN
#unsetopt GLOB_SUBST
#unsetopt HASH_CMDS
##unsetopt HASH_DIRS
#unsetopt HIST_ALLOW_CLOBBER
#unsetopt HIST_FCNTL_LOCK
#unsetopt HIST_NO_FUNCTIONS
#unsetopt HIST_NO_STORE
##unsetopt HUP #
#unsetopt IGNORE_BRACES
#unsetopt IGNORE_EOF
#unsetopt KSH_ARRAYS
#unsetopt KSH_AUTOLOAD
#unsetopt KSH_GLOB
#unsetopt KSH_OPTION_PRINT
#unsetopt KSH_TYPESET
#unsetopt KSH_ZERO_SUBSCRIPT
#unsetopt LIST_ROWS_FIRST
#unsetopt LOCAL_OPTIONS
#unsetopt LOCAL_TRAPS
#unsetopt MAGIC_EQUAL_SUBST
#unsetopt MAIL_WARNING
#unsetopt MARK_DIRS
##setopt NOMATCH
#unsetopt NULL_GLOB
setopt NUMERIC_GLOB_SORT
#unsetopt OCTAL_ZEROES
#unsetopt OVERSTRIKE
#unsetopt POSIX_ALIASES
#unsetopt POSIX_BUILTINS
#unsetopt POSIX_IDENTIFIERS
#unsetopt POSIX_STRINGS
#unsetopt PRINT_EXIT_VALUE
#unsetopt PROMPT_BANG
#unsetopt PUSHD_MINUS
#unsetopt PUSHD_SILENT
#unsetopt RC_QUOTES
#unsetopt REC_EXACT
#unsetopt RM_STAR_SILENT
#unsetopt RM_STAR_WAIT
#unsetopt SH_FILE_EXPANSION
#unsetopt SH_GLOB
#unsetopt SH_NULLCMD
#unsetopt SH_OPTION_LETTERS
#unsetopt SH_WORD_SPLIT
#unsetopt SINGLE_LINE_ZLE
#unsetopt SUN_KEYBOARD_HACK
setopt NOTIFY
#unsetopt TRAPS_ASYNC
#unsetopt TYPESET_SILENT
#unsetopt VERBOSE
#unsetopt VI
#unsetopt WARN_CREATE_GLOBAL
#unsetopt XTRACE
#setopt NULLGLOB
setopt PATH_DIRS
setopt PRINT_EIGHT_BIT ##
#setopt PRINT_EXIT_VALUE        # buggy (i.e., piped while. use RPROMPT instead)
setopt PROMPT_CR
setopt PROMPT_PERCENT
setopt PROMPT_SP
setopt PROMPT_SUBST
setopt PUSHD_IGNORE_DUPS ##
setopt PUSHD_TO_HOME
setopt PUSHD_SILENT #
#setopt PUSHD_MINUS #
#setopt REC_EXACT               # buggy
setopt RC_EXPAND_PARAM
setopt RCS ##
setopt RC_QUOTES #
setopt REMATCH_PCRE ##
setopt SHARE_HISTORY
setopt SHORT_LOOPS ##
setopt TRANSIENT_RPROMPT
setopt UNSET ##
setopt ZLE ##


unsetopt NOMATCH #
#unsetopt HIST_SAVE_BY_COPY # dangerous, but can help with dirs, that needs ssh-chroot
# I hate shell beeping :-/
unsetopt BEEP
unsetopt HIST_BEEP
unsetopt LIST_BEEP


zmodload zsh/zpty #for nvim's deoplete-zsh
zmodload zsh/complist
zmodload zsh/termcap
zmodload -a -i zsh/stat stat
zmodload -a -i zsh/zpty zpty
zmodload -a -i zsh/zprof zprof
zmodload -ap -i zsh/mapfile mapfile

#( (( $+commands[man] )) && (( $+commands[vim] )) ) && {
#  autoload -Uz +X man
#}
(( $+commands[curl] )) && {
  autoload -Uz +X mvapaste
  autoload -Uz +X imgur
}
autoload -Uz +X dice

#autoload -Uz +X dice
#autoload -Uz +X v

# ----------------------------------------------------------------------------
# Aliases
# ----------------------------------------------------------------------------

alias grep='nocorrect grep --color=auto --exclude="*.pyc" --exclude-dir=".svn" --exclude-dir=".hg" --exclude-dir=".bzr" --exclude-dir=".git"';

# --group-directories-first
alias ls='ls -Fh --color=auto';
alias ll='ls -Fhl --color=auto';
alias la='ls -FhlA --color=auto';
alias li='ls -Fhial --color=auto';
#alias lsd='ls -Fhld --color=auto *(-/DN)';
alias lsa='ls -Fhld --color=auto .*';

# cp/mv/rm
alias cp='nocorrect cp -i'
alias mv='nocorrect mv -i'
alias rm='nocorrect rm -I'

#alias ...="cd ../..";

alias rehash="hash -r";

(( $+commands[rsync] )) && alias cpv="rsync -poghb --backup-dir=/tmp/rsync -e /dev/null --progress --"
alias mmv="noglob zmv -M -W -o-i 2>>( grep -v 'z..:.*no wildcards' 1>&2 )";
alias mcp="noglob zmv -C -W -o-i 2>>( grep -v 'z..:.*no wildcards' 1>&2 )";
alias mln="noglob zmv -L -W -o-i 2>>( grep -v 'z..:.*no wildcards' 1>&2 )";

(( $+commands[base64] )) && {
  encode64(){ echo -n "${*}" | base64 }
  decode64(){ echo -n "${*}" | base64 --decode }
  alias e64=encode64
  alias d64=decode64
}

alias -s {html,htm,pdf,djvu,doc,rtf,odt,chm,jpg,jpeg,gif,png,avi,mpeg,mpg,mov,wmv,flv,iflv,rm,vob,ac3,wav,mkv,mov,divx,xvid}='xdg-open'

# Установка глобальных псевдонимов
# для командных конвейеров
alias -g M='|more'
alias -g L='|less'
alias -g H='|head'
alias -g T='|tail'
alias -g N='2>/dev/null'

#-------------------------------------------------------------------------------
# Some additional settings
#-------------------------------------------------------------------------------

# Command not found
test -f /etc/zsh_command_not_found && . /etc/zsh_command_not_found

# predicted autocompletion on command input # autoload -U predict-on
autoload -U zargs
autoload -U zmv
autoload -U zed
autoload -U zcalc
autoload -U zftp

# predicted autocompletion on command input
# zstyle ':predict' verbose 0

# predicted autocompletion on command input # zle -N predict-on
# predicted autocompletion on command input # zle -N predict-off
# predicted autocompletion on command input # zle-line-init() { predict-on }
# predicted autocompletion on command input # zle -N zle-line-init


# ------------------------------------------------------------------------------
# Key bindings / ZLE configuration
# ------------------------------------------------------------------------------

# Use Emacs line editing mode
bindkey -e

# Common key bindings
bindkey "^[[2~"   yank                                    # <insert>        => Insert the contents of the kill buffer at the cursor position.
bindkey '^[[3~'   delete-char-or-list                     # <del>           => Delete next char
bindkey '^[[1;5D' emacs-backward-word                     # <ctrl><left>    => Previous word
bindkey '^[OD'    emacs-backward-word                     # <ctrl><left>    => Previous word (Tmux hack)
bindkey '^[[1;5C' emacs-forward-word                      # <ctrl><right>   => Next word
bindkey '^[OC'    emacs-forward-word                      # <ctrl><right>   => Next word (Tmux hack)
bindkey '^[[3;5~' delete-word                             # <ctrl><del>     => Delete next word
bindkey ' '       magic-space                             # <space>         => Perform history expansion
bindkey "^[e"     expand-cmd-path                         # <meta><e>       => Expand command to it's path
bindkey "^E"      expand-cmd-path                         # <ctrl><e>       => Expand command to it's path

bindkey "^[[5~"   up-line-or-history                      # <PgUp>          =>
bindkey "^[[6~"   down-line-or-history                    # <PgDn>          =>

bindkey "^[[1~"   beginning-of-line                       # <Home> (tty)    =>
bindkey "^[[4~"   end-of-line                             # <End>  (tty)    =>

bindkey "^[[H"    beginning-of-line                       # <Home>          =>
bindkey "^[[F"    end-of-line                             # <End>           =>

bindkey "^[OH"    beginning-of-line                       # <Home>          =>
bindkey "^[OF"    end-of-line                             # <End>           =>

autoload history-search-end && {
  zle -N history-beginning-search-backward-end history-search-end
  zle -N history-beginning-search-forward-end history-search-end
  bindkey "^[[A"    history-beginning-search-backward-end # <up>            => Search command in history (to beginning)
  bindkey "^[[B"    history-beginning-search-forward-end  # <down>          => Search command in history (to the end)
}

##autoload -U tetris && {
## zle -N tetris
##bindkey "^T"      tetris                                # <ctrl><t>         => Tetris
##}

#bindkey '^I'      complete-word
#expand-or-complete-with-dots() {
#  echo -n "..."
#  zle expand-or-complete
#  zle redisplay
#}
#zle -N expand-or-complete-with-dots # does dots needed?
bindkey "^I"      expand-or-complete
#-with-dots          # <ctrl><i>|<Tab>   => Completion
bindkey '^U'      backward-kill-line                    # <ctrl><u>         => Kill line like in bash (all before cursor, not all the line)
bindkey "^[u"     undo                                  # <meta><u>         => Undo
bindkey "^[r"     redo                                  # <meta><r>         => Redo
bindkey "^_"      describe-key-briefly                  # <ctrl></>         => Describe keybinding

## Hooks.
#autoload -U add-zsh-hook && {
#
#}


reconfigure() {
  source "${HOME}/.zshrc"
}
zle -N reconfigure
#bindkey "^[[15~" reconfigure
bindkey $terminfo[kf5] reconfigure

#autoload -U <func> && {
#  zle -N <func>
#  bindkey '<keys>' <func>
#}

# ..../ => ../../../
#autoload -U rationalize-dots && {
#  zle -N rationalize-dots
#  bindkey . rationalize-dots
#  bindkey -M isearch . self-insert &>/dev/null # Exclude from isearch
#}

# Autoquote URLs pasted in ZLE
autoload -U url-quote-magic && {
  zle -N self-insert url-quote-magic
}

# Save cancelled commands to history (unneded for me)
#TRAPINT () {
#  zle && test $HISTNO -eq $HISTCMD && print -rs -- $BUFFER
#  return $1
#}

# <ctrl><x> <x> => enter a password that will be hidden in the buffer and
# shell history.

autoload -U enter-password && {
  zle -N enter-password
  bindkey '^Xx' enter-password
}

# ------------------------------------------------------------------------------
# Colors
# ------------------------------------------------------------------------------

# Not all terms like colors stuff...
if [[ $TERM == "dumb" ]] {
    export PS1='%(?..[%?])%!:%~%# '
    unsetopt zle
    unsetopt prompt_cr
    unsetopt prompt_subst
    unfunction precmd &>/dev/null
    unfunction preexec &>/dev/null
} elif [[ $TERM == "linux" ]] {
  autoload -Uz colors && colors
  (( $+commands[dircolors] )) && eval `dircolors -b`
  autoload -Uz promptinit && promptinit -z
  CHOSEN_PROMPT=${CHOSEN_PROMPT:-gentoovcs}
  # "Смайлик", означающий статус завершения предыдущей команды (улыбается, если 0 и грустит, если нет)
  RPROMPT="%(?,%{$fg[green]%}:%)%{$reset_color%},%{$fg[red]%}:(%{$reset_color%}"
} else {
  # Effects array, 256 colors foreground/background arrays,
  autoload -Uz colors && colors
  typeset -Ag FX FG BG
  if (( $+commands[tput] )) {
    FX=(
     reset     "$(tput sgr0)"
     bold      "$(tput bold)"
     italic    "$(tput sitm)"  no-italic    "$(tput ritm)"
     underline "$(tput smul)"  no-underline "$(tput rmul)"
     blink     "$(tput blink)"
     reverse   "$(tput rev)"   no-reverse   "$(tput rum)"
     standout  "$(tput smso)"  no-standout  "$(tput rmso)"
    )
  } else {
    FX=( # TODO: !!!
     reset     "$(tput sgr0)"
     bold      "$(tput bold)"
     italic    "$(tput sitm)"  no-italic    "$(tput ritm)"
     underline "$(tput smul)"  no-underline "$(tput rmul)"
     blink     "$(tput blink)"
     reverse   "$(tput rev)"   no-reverse   "$(tput rum)"
     standout  "$(tput smso)"  no-standout  "$(tput rmso)"
    )
  }

  # 256-color; # TODO: think about replacing with not-so-heavy function
  for color ({0..255}) {
    FG[$color]="[38;5;${color}m"
    BG[$color]="[48;5;${color}m"
  }

  # ^ would be too heavy table for true-color

  FG[none]=$FG[0];        BG[none]=$BG[0]
  FG[darkred]=$FG[1];     BG[darkred]=$BG[1]
  FG[darkgreen]=$FG[2];   BG[darkgreen]=$BG[2]
  FG[darkyellow]=$FG[3];  BG[darkyellow]=$BG[3]
  FG[darkblue]=$FG[4];    BG[darkblue]=$BG[4]
  FG[darkmagenta]=$FG[5]; BG[darkmagenta]=$BG[5]
  FG[darkcyan]=$FG[6];    BG[darkcyan]=$BG[6]
  FG[grey]=$FG[7];        BG[grey]=$BG[7]
  FG[darkgrey]=$FG[8];    BG[darkgrey]=$BG[8]
  FG[gray]=$FG[7];        BG[gray]=$BG[7]
  FG[darkgray]=$FG[8];    BG[darkgray]=$BG[8]
  FG[red]=$FG[9];         BG[red]=$BG[9]
  FG[green]=$FG[10];      BG[green]=$BG[10]
  FG[yellow]=$FG[11];     BG[yellow]=$BG[11]
  FG[blue]=$FG[12];       BG[blue]=$BG[12]
  FG[magenta]=$FG[13];    BG[magenta]=$BG[13]
  FG[cyan]=$FG[14];       BG[cyan]=$BG[14]
  FG[lightgrey]=$FG[15];  BG[lightgrey]=$BG[15]
  FG[lightgray]=$FG[15];  BG[lightgray]=$BG[15]
  FG[darkred256]=$FG[52]; BG[darkred256]=$BG[52]

  # ls colorizing with dircolors.
  (( $+commands[dircolors] )) && {
    eval `TERM=xterm-256color dircolors -b`

    # Add some more colors!
    function _add_lscolors() {
      local color ext mimetype
      mimetype=shift; color=shift

      test -f ${EPREFIX}/etc/mime.types &&
      (( $+commands[grep] )) && (( $+commands[cut] )) &&
      for ext ($(grep "$mimetype" ${EPREFIX}/etc/mime.types | cut -f 2-)) {
        LS_COLORS="$LS_COLORS:*.$ext=$color"
      }
      export LS_COLORS
    }
    _add_lscolors "audio/" "01;38;05;132"
    _add_lscolors "image/" "01;38;05;170"
    _add_lscolors "video/" "01;38;05;197"

    eval $(dircolors -b -< "${CONFIG_HOME}"/.dir_colors)
  } || {
    # TODO: think about unhardcoding even for non-dircolors-compat systems
    LS_COLORS='no=00:fi=00:di=00;38;05;24:ln=target:pi=38;05;142;48;05;235:so=01;38;05;176:do=01;38;05;176:bd=38;05;142;48;05;235;01:cd=38;05;142;48;05;235;01:or=01;06;38;05;254;48;05;124:mi=01;05;38;05;254;48;05;124:su=38;05;255;48;05;124:sg=38;05;237;48;05;221:tw=38;05;16;48;05;41:ow=34;42:st=01;38;05;254;48;05;21:ex=01;38;05;46:*.tar=00;38;05;33:*.tgz=00;38;05;33:*.txz=00;38;05;33:*.tbz=00;38;05;33:*.tlz=00;38;05;33:*.arj=00;38;05;33:*.taz=00;38;05;33:*.lzh=00;38;05;33:*.lzma=00;38;05;33:*.zip=00;38;05;33:*.z=00;38;05;33:*.Z=00;38;05;33:*.dz=00;38;05;33:*.gz=00;38;05;33:*.bz2=00;38;05;33:*.bz=00;38;05;33:*.tbz2=00;38;05;33:*.lz=00;38;05;33:*.xz=00;38;05;33:*.tz=00;38;05;33:*.deb=00;38;05;33:*.rpm=00;38;05;33:*.jar=00;38;05;33:*.rar=00;38;05;33:*.ace=00;38;05;33:*.zoo=00;38;05;33:*.cpio=00;38;05;33:*.7z=00;38;05;33:*.rz=00;38;05;33:*.sqfs=00;38;05;33:*.sqfs3=00;38;05;33:*.db=00;38;05;33:*.sqlite=00;38;05;33:*.sql=00;38;05;33:*.iso=00;38;05;33:*.pdb=00;38;05;33:*.img=00;38;05;33:*.gho=00;38;05;33:*.yml=00;38;05;33:*.bib=00;38;05;33:*.jpg=00;38;05;170:*.eps=00;38;05;170:*.JPG=00;38;05;170:*.jpeg=00;38;05;170:*.gif=00;38;05;170:*.bmp=00;38;05;170:*.pbm=00;38;05;170:*.pgm=00;38;05;170:*.ppm=00;38;05;170:*.tga=00;38;05;170:*.xbm=00;38;05;170:*.xpm=00;38;05;170:*.tif=00;38;05;170:*.tiff=00;38;05;170:*.png=00;38;05;170:*.svg=00;38;05;170:*.svgz=00;38;05;170:*.mng=00;38;05;170:*.pcx=00;38;05;170:*.mov=00;38;05;197:*.mpg=00;38;05;197:*.mpeg=00;38;05;197:*.m2v=00;38;05;197:*.mkv=00;38;05;197:*.ogm=00;38;05;197:*.ogx=00;38;05;197:*.mp4=00;38;05;197:*.m4v=00;38;05;197:*.mp4v=00;38;05;197:*.vob=00;38;05;197:*.qt=00;38;05;197:*.nuv=00;38;05;197:*.wmv=00;38;05;197:*.ogv=00;38;05;197:*.asf=00;38;05;197:*.rm=00;38;05;197:*.rmvb=00;38;05;197:*.flc=00;38;05;197:*.flv=00;38;05;197:*.avi=00;38;05;197:*.AVI=00;38;05;197:*.3gp=00;38;05;197:*.fli=00;38;05;197:*.gl=00;38;05;197:*.dl=00;38;05;197:*.xcf=00;38;05;197:*.xwd=00;38;05;197:*.yuv=00;38;05;197:*.pdf=00;38;05;231:*.ps=00;38;05;123:*.patch=00;38;05;123:*.diff=00;38;05;123:*.log=00;38;05;123:*.doc=00;38;05;231:*.chm=00;38;05;123:*.css=00;38;05;123:*.dsl=00;38;05;123:*.ebuild=00;38;05;123:*.htm=00;38;05;231:*.html=00;38;05;231:*.odb=00;38;05;123:*.odf=00;38;05;231:*.odg=00;38;05;123:*.odp=00;38;05;123:*.ods=00;38;05;123:*.odt=00;38;05;123:*.rnc=00;38;05;123:*.rng=00;38;05;123:*.sgml=00;38;05;123:*.xml=00;38;05;123:*.xsl=00;38;05;123:*.rst=01;38;05;147:*.lyx=01;38;05;147:*.mkd=01;38;05;147:*.mdown=01;38;05;147:*.markdown=01;38;05;147:*.md=01;38;05;147:*.tex=01;38;05;147:*.txt=01;38;05;123:*.pgp=01;38;05;123:*.asc=01;38;05;123:*INSTALL=01;38;05;123:*FAQ=01;38;05;123:*TODO=01;38;05;123:*README=01;38;05;123:*README.txt=01;38;05;123:*README.md=01;38;05;123:*README.markdown=01;38;05;123:*readme.txt=01;38;05;123:*COPYING=01;38;05;123:*LICENSE=01;38;05;123:*.aac=01;38;05;132:*.au=01;38;05;132:*.flac=01;38;05;132:*.gsf=01;38;05;132:*.h2song=01;38;05;132:*.mid=01;38;05;132:*.midi=01;38;05;132:*.mka=01;38;05;132:*.mp3=01;38;05;132:*.mpc=01;38;05;132:*.ogg=01;38;05;132:*.oga=01;38;05;132:*.ra=01;38;05;132:*.rg=01;38;05;132:*.tta=01;38;05;132:*.wav=01;38;05;132:*.wma=01;38;05;132:*.cfg=00;38;05;227:*.conf=00;38;05;227:*rc=00;38;05;227:*.ini=00;38;05;227:*.reg=00;38;05;227:*.F=00;38;05;172:*.c=00;38;05;172:*.cc=00;38;05;172:*.cpp=00;38;05;172:*.cxx=00;38;05;172:*.f=00;38;05;172:*.f90=00;38;05;172:*.h=00;38;05;172:*.hs=00;38;05;172:*.lua=00;38;05;172:*.mp=00;38;05;172:*.pl=00;38;05;172:*.po=00;38;05;172:*.php=00;38;05;172:*.php5=00;38;05;172:*.py=00;38;05;172:*.sh=00;38;05;172:*.bash=00;38;05;172:*.zsh=00;38;05;172:*zshrc=00;38;05;172:*zshenv=00;38;05;172:*zprofile=00;38;05;172:*.gmo=00;38;05;172:*.ko=00;38;05;172:*.mo=00;38;05;172:*.o=00;38;05;172:*.rb=00;38;05;172:*Makefile=00;38;05;172:*Makefile*=00;38;05;172:*CMakeLists.txt=00;38;05;172:*.cmake=00;38;05;172:*.java=00;38;05;172:*.as=00;38;05;172:*.mxml=00;38;05;172:*.groovy=00;38;05;172:*.gtmpl=00;38;05;172:*pom.xml=00;38;05;172:*build.xml=00;38;05;172:*.flex=00;38;05;172:*.cup=00;38;05;172:*.btm=00;38;05;172:*.gitignore=00;38;05;245:*.gitattributes=00;38;05;245:*.gitmodules=00;38;05;245:*.svnignore=00;38;05;245:*.hgignore=00;38;05;245:*.hgtags=00;38;05;245:*.settings=00;38;05;245:*.project=00;38;05;245:*.classpath=00;38;05;245:*.iml=00;38;05;245:*.iws=00;38;05;245:*.ipr=00;38;05;245:*~=00;38;05;008:*.exe=00;38;05;46:*.bin=00;38;05;26:*.dat=00;38;05;26:*.trx=00;38;05;26:*.anx=01;35:*.axa=00;36:*.axv=01;35:*.cgm=01;35:*.emf=01;35:*.spx=00;36:*.xspf=00;36';
    export LS_COLORS;
  }

  # Prompt
  autoload -Uz promptinit && promptinit -z
  CHOSEN_PROMPT=${CHOSEN_PROMPT:-powerline}

  # less: colors
  export LESS_TERMCAP_mb=$FX[bold]$FG[blue]
  export LESS_TERMCAP_md=$FX[bold]$FG[blue]
  export LESS_TERMCAP_me=$FX[reset]
  export LESS_TERMCAP_so=$BG[darkgrey]$FG[lightgrey]
  export LESS_TERMCAP_se=$FX[reset]
  export LESS_TERMCAP_us=$FX[underline]$FG[white]
  export LESS_TERMCAP_ue=$FX[no-underline]$FX[reset]
  export LESS_TERMCAP_mr=$FX[reverse]$FX[bold]
  export LESS_TERMCAP_mh=$FX[bold]

  # less: syntax highlighting
#  if (( $+commands[src-hilite-lesspipe.sh] )); then
#    export LESS="${LESS} --RAW-CONTROL-CHARS"
#    export LESSOPEN="| src-hilite-lesspipe.sh %s"
  (( $+commands[lesspipe.sh] )) && {
    export LESS="${LESS} --RAW-CONTROL-CHARS"
    export LESSOPEN="| lesspipe.sh %s"
    export LESSCOLOR="always"
##    eval $(lesspipe.sh)
  }

  # Colorizing with grc.
#  (( $+commands[grc] )) && {
#    alias @diff='command diff'             && alias diff='grc diff'
#    alias @make='command make'             && alias make='grc make'
#    alias @gcc='command gcc'               && alias gcc='grc gcc'
#    alias @g++='command g++'               && alias g++='grc g++'
#    alias @ld='command ld'                 && alias ld='grc ld'
#    alias @netstat='command netstat'       && alias netstat='grc netstat'
#    alias @ping='command ping'             && alias ping='grc ping'
#    alias @cvs='command cvs'               && alias cvs='grc cvs'
#    alias @traceroute='command traceroute' && alias traceroute='grc traceroute'
#  }

  # Colorizing with rainbow.
#  (( $+commands[rainbow] )) && {
#    alias @mvn='command mvn'               && alias mvn='rainbow --config mvn3 -- mvn'
#    alias @mvn2='command mvn2'             && alias mvn2='rainbow --config mvn2 -- mvn2'
#    alias @diff='command diff'             && alias diff='rainbow -- diff'
#    alias @df='command df'                 && alias df='rainbow -- df'
#    alias @host='command host'             && alias host='rainbow -- host'
#    alias @ifconfig='command ifconfig'     && alias ifconfig='rainbow -- ifconfig'
#    alias @md5sum='command md5sum'         && alias md5sum='rainbow -- md5sum'
#    alias @ping='command ping'             && alias ping='rainbow -- ping'
#    alias @top='command top'               && alias top='rainbow -- top'
#    alias @traceroute='command traceroute' && alias traceroute='rainbow -- traceroute'
#    alias @jonas='command jonas'           && alias jonas='rainbow -- jonas'
#  }

  # Colorizing with color(diff|svn|cvs|gcc|make).
#  (( $+commands[colordiff] )) && alias @diff='command diff' && alias diff='colordiff'
#  (( $+commands[colorsvn] ))  && alias @svn='command svn'   && alias svn='colorsvn'
#  (( $+commands[colorcvs] ))  && alias @cvs='command cvs'   && alias cvs='colorcvs'
#  (( $+commands[colormake] )) && alias @make='command make' && alias make='colormake'
#  (( $+commands[colorgcc] ))  && [[ -d /usr/lib/colorgcc/bin ]] && path=(/usr/lib/colorgcc/bin $path)

  # zsh-syntax-highlighting
#  source "${CONFIG_HOME}"/config/zsh/bundles/mva/apps/zsh-syntax-highlighting.git/zsh-syntax-highlighting.zsh &>/dev/null && {
#    source "${CONFIG_HOME}"/.config/zsh/bundles/mva/settings/highlight_setup.zsh
#  }
}


# ------------------------------------------------------------------------------
# Completion
# ------------------------------------------------------------------------------

autoload -Uz compinit && {
  # Init completion, ignoring security checks.
  compinit -u

  # cd style.
  zstyle ':completion:*:*:cd:*' tag-order local-directories directory-stack path-directories

  # rm/cp/mv style.
  zstyle ':completion:*:(rm|mv|scp|ls):*' ignore-line yes
#  zstyle ':completion:*:*:(^rm):*:*files' ignored-patterns '*?.o' '*?.c~' '*?.old' '*?.pro'

  # kill style.
  zstyle ':completion:*:processes' command 'ps -w -w -a -u $(whoami) -o pid,cmd --sort=-pid | sed "/ps/d"'
  zstyle ':completion:*:processes' insert-ids menu yes select
  zstyle ':completion:*:processes-names' command "ps $([[ $UID == 0 ]] && echo a) h x -o command | sed -r -e '/(^\[.*\]$|^init|^-|^ps|^sed)/d'"
#  zstyle ':completion:*:processes-names' command 'ps xho command|sed "s/://g"'
  zstyle ':completion:*:processes' sort false

# TODO: 24bit colors ⇓
  zstyle ':completion:*:processes' list-colors "=(#b) #([0-9]#)*=38;5;14;1=38;5;9;1"
#'=(#b) #([0-9]#)*=0=01;31' # somewhy stop working

  # Hostnames completion.
  zstyle -e ':completion:*:hosts' hosts 'reply=(
    ${${${${${(f)"$(cat ${HOME}/.ssh/known_hosts 2>/dev/null)"//\[/}//\]:/ }:#[\|]*}%%\ *}%%,*}
    ${${${(z)${(@M)${(f)"$(cat ${HOME}/.ssh/config 2>/dev/null)"}:#Host *}#Host }:#*[*?]*}}
    ${${${(z)${(@M)${(f)"$(cat ${HOME}/.ssh/cfg/* 2>/dev/null)"}:#Host *}#Host }:#*[*?]*}}
    ${(s: :)${(ps:\t:)${${(f)~~"$(cat /etc/hosts 2>/dev/null)"}%%\#*}#*[[:blank:]]}}
  )'
#    ${${${(@M)${(f)"$(cat ${HOME}/.ssh/config 2>/dev/null)"}:#Host *}#Host }:#*[*?]*}
  zstyle ':completion:*:*:*:hosts' ignored-patterns 'ip6*' 'localhost*'

  zstyle ':completion:*:sudo::' environ PATH="/usr/local/sbin:/usr/sbin:/sbin:$PATH:/opt/bin" HOME="${EPREFIX}/root"
#  zstyle ':completion:*:urls' local 'www' '/var/www/htdocs' 'public_html'


#
#  # Completion debugging
#  bindkey '^Xh' _complete_help
#  bindkey '^X?' _complete_debug
#

# Cache setup.
  zstyle ':completion:*' use-cache on
  zstyle ':completion::complete:*' '\\'
  zstyle ':completion::complete:*' cache-path "${HOME}/.zcompcache"
  zstyle ':completion::complete:*' use-cache on

  zstyle ':completion:incremental:*' completer _complete _correct
  zstyle ':completion:*:complete:-command-::commands' ignored-patterns '*\~'

  zstyle ':completion:*:expand:*' tag-order all-expansions
#zstyle ':completion:*:expand:*' group-order all-expansions original expansions
#zstyle ':completion:*:expand:*' tag-order 'all-expansions original expansions'

  zstyle ':completion:*:matches' group yes
  zstyle ':completion:*:match:*' original only
  zstyle ':completion:*:options' auto-description '%d'
  zstyle ':completion:*:options' description yes

  zstyle ':completion:predict:*' completer _complete
  zstyle ':completion::prefix-1:*' completer _complete
  zstyle ':completion:*:*:-subscript-:*' tag-order indexes parameters
#  zstyle -e ':completion:*:approximate:*' max-errors 'reply=( $(( ($#PREFIX+$#SUFFIX)/3 )) numeric )'
  zstyle -e ':completion:*:approximate:*' max-errors 'reply=( $(( ($#PREFIX+$#SUFFIX)/3 )) numeric )'

  zstyle ':completion:*' expand prefix suffix
  zstyle ':completion:*' expand 'yes'
  zstyle ':completion:*' group-name ''
  zstyle ':completion:*' list-prompt "%SСейчас на %p. Нажми TAB, чтобы листать далее или следующий символ для подстановки%s"
  zstyle ':completion:*' list-suffixes yes
#zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z} r:|[._-]=** r:|=**' 'm:{a-z}={A-Z} m:{a-zA-Z}={A-Za-z} r:|[._-]=** r:|=** l:|=*' 'm:{a-zA-Z}={A-Za-z} r:|[._-]=** r:|=** l:|=*' 'm:{a-zA-Z}={A-Za-z} r:|[._-]=** r:|=** l:|=*'
#zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z} r:|[._-]=** r:|=** l:|=*' 'm:{a-z}={A-Z} m:{a-zA-Z}={A-Za-z} r:|[._-]=** r:|=** l:|=*'



zstyle ':completion:*' matcher-list 'm:{a-zA-Zа-яА-Яα-ωΑ-Ω}={A-Za-zА-Яа-яΑ-Ωα-ω} r:[a-zA-Zа-яА-Яα-ωΑ-Ω]||[A-Za-zА-Яа-яΑ-Ωα-ω]=* r:|[._-]=* r:|=* l:|=*'

zstyle '*' single-ignored show

#zstyle ':completion:*' matcher-list 'm:{a-zA-Zа-яА-Яα-ωΑ-Ω}={A-Za-zА-Яа-яΑ-Ωα-ω} r:[a-zA-Zа-яА-Яα-ωΑ-Ω]||[A-Za-zА-Яа-яΑ-Ωα-ω]=** r:|[._-]=** r:|=** l:|=*'
#zstyle ':completion:*' matcher-list 'm:{a-zA-Zа-яА-Яα-ωΑ-Ω}={A-Za-zА-Яа-яΑ-Ωα-ω} r:[a-zA-Zа-яА-Яα-ωΑ-Ω]||[A-Za-zА-Яа-яΑ-Ωα-ω]=**'
##zstyle ':completion:*' matcher-list 'm:{a-zA-Zа-яА-Яα-ωΑ-Ω}={A-Za-zА-Яа-яΑ-Ωα-ω}'
##zstyle ':completion:*' matcher-list 'r:[a-zA-Zа-яА-Яα-ωΑ-Ω]||[A-Za-zА-Яа-яΑ-Ωα-ω]=**'
  zstyle ':completion:*' original yes
  zstyle ':completion:*' accept-exact-dirs yes
  zstyle ':completion:*' preserve-prefix '//[^/]##/'
  zstyle ':completion:*' select-prompt "%SСкроллинг активен. Текущее выделение на: %p%s"
  zstyle ':completion:*' special-dirs yes
  zstyle ':completion:*' squeeze-slashes yes
  zstyle ':completion:*' substitute 1
  zstyle ':completion:*' use-compctl yes
  zstyle ':completion:*' verbose yes
  zstyle ':completion:*' word yes

  # Speed up completion by avoiding partial globs.
  zstyle ':completion:*' accept-exact '*(N)'
  zstyle ':completion:*' accept-exact-dirs true

  # Default colors for listings.
##  zstyle -e ':completion:*:default' list-colors 'reply=("${PREFIX:+=(#bi)($PREFIX:t)(?)(${(s.:.)LS_COLORS//=/)==32=34=}}")'
##  zstyle -e ':completion:*'         list-colors 'reply=("${PREFIX:+=(#bi)($PREFIX:t)(?)(${(s.:.)LS_COLORS//=/)==32=34=}}")'
#  zstyle -e ':completion:*:default' list-colors 'reply=("${PREFIX:+=(#bi)($PREFIX:t)(?)(*)==32=34=31}:${(s.:.)LS_COLORS}")'
#  zstyle -e ':completion:*'         list-colors 'reply=("${PREFIX:+=(#bi)($PREFIX:t)(?)(*)==32=34=31}:${(s.:.)LS_COLORS}")'

#  zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
#  zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

#*:*:(^rm):*:*
# =(#b) #([0-9]#)*=38;5;14;1=38;5;9;1

  zstyle ':completion:*:*directories' list-colors "${(M@)${(s.:.)LS_COLORS}:#(di|ln|su|sg|tw|ow|st|mh)=*}"
  zstyle ':completion:*:*files' list-colors "${(R@)${(s.:.)LS_COLORS}:#(no|rs)=*}"
  zstyle ':completion:*:argument-rest:*' list-colors "${(R@)${(s.:.)LS_COLORS}:#(no|rs)=*}"
  zstyle ':completion:*:argument-1:*' list-colors "${(R@)${(s.:.)LS_COLORS}:#(no|rs)=*}"

#  zstyle ':completion:*:argument-1:*directories' list-colors "${(M@)${(s.:.)LS_COLORS}:#(di|ln|su|sg|tw|ow|st|mh)=*}"
#  zstyle ':completion:*:argument-1:*files' list-colors "${(R@)${(s.:.)LS_COLORS}:#(no|rs)=*}"

# FIXME!
# ref: http://www.zsh.org/mla/users/2017/msg00240.html
  zstyle ':completion:*:hosts' list-colors 'no=32'
  zstyle ':completion:*:users' list-colors 'no=31'

#  zstyle ':completion:*' list-colors "${(R@)${(s.:.)LS_COLORS}:#(no|rs)=*}"
#  zstyle ':completion:*:default' list-colors "${(R@)${(s.:.)LS_COLORS}:#(no|rs)=*}"

# BUG:
#  zstyle ':completion:*' list-colors "no=30;1"
#  zstyle ':completion:*:default' list-colors "no=38;5;244;1"

  # Separate directories from files.
  zstyle ':completion:*' list-dirs-first true

  # Turn on menu selection always
  zstyle ':completion:*' menu true select
    # only when selections do not fit on screen:
    #  zstyle ':completion:*' menu true=long select=long

  # Separate matches into groups.
  zstyle ':completion:*:matches' group true
  zstyle ':completion:*' group-name ''

  # Always use the most verbose completion.
  zstyle ':completion:*' verbose true

  # Treat sequences of slashes as single slash.
  zstyle ':completion:*' squeeze-slashes true

  # Describe options.
  zstyle ':completion:*:options' description true

  # Completion presentation styles.
  zstyle ':completion:*:options' auto-description '%d'
  zstyle ':completion:*:corrections' format '%B%d (ошибки: %e)%b'
  zstyle ':completion:*:descriptions' format "$fg_bold[yellow] %B%d%b $reset_color"
  zstyle ':completion:*:messages' format "$fg_bold[magenta] %d $reset_color"
  zstyle ':completion:*:warnings' format "$fg_bold[red] Нет совпадений с$reset_color %d"

#  # Ignore hidden files by default
#  zstyle ':completion:*:(all-|other-|)files'  ignored-patterns '*/.*'
#  zstyle ':completion:*:(local-|)directories' ignored-patterns '*/.*'
#  zstyle ':completion:*:cd:*'                 ignored-patterns '*/.*'

  # Don't complete completion functions/widgets.
  zstyle ':completion:*:functions' ignored-patterns '_*'

  # Show ignored patterns if needed.
  zstyle '*' single-ignored show


  # Force rehash to have completion picking up new commands in path.
  _force_rehash() { (( CURRENT == 1 )) && rehash; return 1 }

#zstyle ':completion:*' completer _complete _prefix _correct _prefix _match _approximate
#zstyle ':completion:*::::' completer _expand _complete _ignored _approximate

  zstyle ':completion:::::' completer _force_rehash \
                                      _expand \
                                      _complete \
                                      _prefix \
                                      _ignored \
                                      _approximate \
#                                      _correct \
#                                      _gnu_generic

  zstyle ':completion:*'    completer _force_rehash \
                                      _complete \
                                      _ignored \
                                      _prefix \
                                      _approximate \
#                                      _correct \
#                                      _oldlist \
#                                      _gnu_generic
}

# ------------
# Bash completions
# ------------
autoload -Uz bashcompinit && bashcompinit -u

# ------------------------------------------------------------------------------
# Site-specific stuff
# ------------------------------------------------------------------------------

distro_is() (
  (( $+commands[grep] )) &&
  grep -qi "${1}" /etc/*release '' &>/dev/null
)

# Distro-specific stuff
for dis ("${CONFIG_HOME}"/.cond.bin/*) {
  local d=${dis%%/} dd="${dis:t}"
  if ($(distro_is "${dis:t}")) {
    [[ -d "${d}" ]] && path=("${d}" "${path[@]}")
  } elif [[ "${dd}" == "ruby" ]] && (( $+commands[ruby] )) {
    (( ____cls_loaded )) && {
        (( $+commands[colorls] )) &&
        alias cls="colorls" ||
        alias cls="${CONFIG_HOME}/.cond.bin/ruby/colorls"
        unset ____cls_loaded
    }
  } elif [[ "${dd}" == "vim" ]] && ( (( $+commands[nvim] )) || (( $+commands[vim] )) ) {
    [[ -d "${d}" ]] && path=("${d}" "${path[@]}")
  } elif [[ "${dd}" == "gpg" ]] && ( (( $+commands[gpg2] )) || (( $+commands[gpg] )) ) {
    [[ -d "${d}" ]] && path=("${d}" "${path[@]}")
  }
  unset d dd dis # not a subshell, so even locals goes to shell level
}

# Local/modularized stuff.
for _conf ("${CONFIG_HOME}"/.config/zsh/bundles/mva/zshrc.d/*.(|ba|z|tc|k)sh(#q.N)) source ${_conf}

# ------------------------------------------------------------------------------
# Performance
# ------------------------------------------------------------------------------

# Recompile if needed.
autoload -U zrecompile && zrecompile -p "${HOME}"/.{zcompdump,zshrc} &>/dev/null

if [[ -f "${HOME}/.zshrc_custom" ]] {
  if { test ! -f "${HOME}/.zshrc_custom.zwc" -o "${HOME}/.zshrc_custom" -nt "${HOME}/.zshrc_custom.zwc" } {
    zcompile "${HOME}/.zshrc_custom"
  }
  source "${HOME}/.zshrc_custom"
}

if [[ -f "${HOME}/.custom.zshrc" ]] {
  if { test ! -f "${HOME}/.custom.zshrc.zwc" -o "${HOME}/.custom.zshrc" -nt "${HOME}/.custom.zshrc.zwc" } {
    zcompile "${HOME}/.custom.zshrc"
  }
  source "${HOME}/.custom.zshrc"
}

#test -z "${ZSH_HIGHLIGHT_VERSION}" && source "${CONFIG_HOME}"/.config/zsh/bundles/mva/apps/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh &>/dev/null;
#source "${CONFIG_HOME}"/.config/zsh/bundles/mva/settings/highlight_setup.zsh
FAST_WORK_DIR=XDG
source "${CONFIG_HOME}"/.config/zsh/bundles/mva/apps/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh

# ------------------------------------------------------------------------------
# UI (themes)
# ------------------------------------------------------------------------------

fast-theme -q ${HLTHEME:-XDG:mva}
prompt ${CHOSEN_PROMPT}


# ------------------------------------------------------------------------------
# Post-load things
# ------------------------------------------------------------------------------

autoload -Uz +X bashcompinit && {
  have() {
    unset have
    (( ${+commands[$1]} )) && have=yes
  }
  bash_source() {
    alias shopt=':'
    alias _expand=_bash_expand
    alias _complete=_bash_comp
    alias _have=have
    alias _realcommand="whence -s"
    emulate -L bash
    setopt kshglob noshglob braceexpand
    source "$@"
# &>/dev/null
    emulate -L zsh
  }
  for _bash_comp_name (${FORCE_BASHCOMPS[@]}) {
    local _bash_comp_path=/usr/share/bash-completion/completions/${_bash_comp_name}
    if [[ -r ${_bash_comp_path} ]] {
      bash_source ${_bash_comp_path}
    }
  }
  unset _bash_comp_path _bash_comp_name _conf

#  for c (/usr/share/bash-completion/completions/*) {
#    cn=${c:t}
#    if [[ -n "${cn:|BASHCOMP_DISABLED}" ]] {
##      emulate bash
#      bash_source "${c}"
##      emulate zsh
#    }
#  }
}

__bg() {
  setopt local_options no_notify no_monitor
  "${@}" &|
}

(( $+commands[pyenv] )) && {
  eval "$(pyenv init -)"
}

# AWS cli
(( ${+commands[aws_zsh_completer.sh]} )) && {
  sched +1 "source $(which aws_zsh_completer.sh) && compinit -u"
}

# heroku
(( ${+commands[heroku]} )) && {
  HEROKU_AC_PATH="${XDG_CACHE_HOME}"/heroku/autocomplete
  HEROKU_AC_ZSH_SETUP_PATH="${HEROKU_AC_PATH}"/zsh_setup
  HEROKU_AC_ANALYTICS_DIR="${HEROKU_AC_PATH}"/completion_analytics
  HEROKU_AC_COMMANDS_PATH="${HEROKU_AC_PATH}"/commands
  HEROKU_AC_ZSH_SETTERS_PATH=${HEROKU_AC_COMMANDS_PATH}_setters
  HEROKU_AC_ZSH_F_PATH="/usr/lib64/node_modules/heroku/node_modules/@heroku-cli/plugin-autocomplete/autocomplete/zsh" # how to properly unhardcode it? :'(
  fpath+=("${HEROKU_AC_ZSH_F_PATH}")
  if [[ -f "${HEROKU_AC_ZSH_SETTERS_PATH}" ]] {
    sched +1 "source ${HEROKU_AC_ZSH_SETTERS_PATH} && compinit -u"
  } else {
    regen_compl() {
      __bg heroku autocomplete -r 0>/dev/null &>/dev/null
    }
    can_source() {
      local cnt=${1:-0}
      (( cnt >= 10 )) && return 1
      if ! [[ -f "${HEROKU_AC_ZSH_SETTERS_PATH}" ]] {
        let cnt+=1
        sched +1 "${0} ${cnt}"
      } else {
        source "${HEROKU_AC_ZSH_SETTERS_PATH}" &&
        compinit -u
      }
    }
    sched +1 "regen_compl"
    sched +1 "can_source"
  }
}

if [[ -f "${HOME}"/.zshrc_custom_post ]] {
  source "${HOME}"/.zshrc_custom_post
}

# vim: ft=zsh sw=2 ts=2 et
