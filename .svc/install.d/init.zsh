#!zsh
I="${${(%):-%N}:A:h}"

emulate zsh
setopt SHORT_LOOPS

declare -a Restore Replace Create ForceDirs

Load() {
	for p ("${@}") {
		local d="${I}/${p}"
		if [[ -d "${d}" ]] {
			for h ("${d}"/*) {
				source "${h}"
			}
		} else {
			source "${d}"
		}
	}
}

Load .config
Load funcs
